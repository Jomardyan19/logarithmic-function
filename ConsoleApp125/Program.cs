﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp125
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 52, 51, 85, 89, 23 };
            int find = Rank(52, arr);
            int find1 = Rank(51, arr);
            int find2 = Rank(85, arr);
            int find3 = Rank(89, arr);
            int find4 = Rank(23, arr);
            Console.WriteLine(find);
            Console.WriteLine(find1);
            Console.WriteLine(find2);
            Console.WriteLine(find3);
            Console.WriteLine(find4);
         
        }

      

        public static int Rank(int key, int[] numbers)
        {
            int low = 0;
            int high = numbers.Length - 1;
            while (low <= high)
            {
                // находим середину
                int mid = low + (high - low) / 2;
                // если ключ поиска меньше значения в середине
                // то верхней границей будет элемент до середины
                if (key < numbers[mid]) high = mid - 1;
                else if (key > numbers[mid]) low = mid + 1;
                else return mid;
            }
            return -1;
        }
    }
}
